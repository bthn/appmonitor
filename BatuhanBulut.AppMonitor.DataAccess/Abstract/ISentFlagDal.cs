﻿using System;
using BatuhanBulut.AppMonitor.Core.DataAccess;
using BatuhanBulut.AppMonitor.Entities;

namespace BatuhanBulut.AppMonitor.DataAccess.Abstract
{
    public interface ISentFlagDal : IEntityRepository<SentFlag>
    {
    }
}
