﻿using System;
using BatuhanBulut.AppMonitor.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BatuhanBulut.AppMonitor.DataAccess.Concrete.EntityFramework
{
    public class MonitorContext : DbContext
    {
        public MonitorContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(
                "Host=localhost;Database=appmonitor;Username=appmonitor;Password=12345678"
                );
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Override default naming convention which uses plural names. Use singular.
            foreach (IMutableEntityType entityType in modelBuilder.Model.GetEntityTypes())
            {
                entityType.SetTableName(entityType.DisplayName());
            }

            modelBuilder.Entity<Application>()
                .HasIndex(e => e.CreateDate);

            modelBuilder.Entity<Application>()
                .HasIndex(e => e.Name);

            modelBuilder.Entity<Application>()
                .HasIndex(e => e.URL);

            modelBuilder.Entity<User>()
                .HasIndex(e => e.Name);

            modelBuilder.Entity<User>()
                .HasIndex(e => e.Surname);

            modelBuilder.Entity<User>()
                .HasIndex(e => e.Email);

            modelBuilder.Entity<User>()
                .HasIndex(e => e.UserName);

            modelBuilder.Entity<User>()
                .HasIndex(e => e.CreateDate);

            modelBuilder.Entity<DownRecord>()
                .HasIndex(e => e.CreateDate);

            modelBuilder.Entity<TargetEmail>()
                .HasIndex(e => e.Email);

            modelBuilder.Entity<TargetEmail>()
                .HasIndex(e => e.CreateDate);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<DownRecord> DownRecords { get; set; }
        public DbSet<TargetEmail> TargetEmails { get; set; }
        public DbSet<SentFlag> SentFlags { get; set; }

    }
}
