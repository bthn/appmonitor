﻿using System;
using BatuhanBulut.AppMonitor.Core.DataAccess.EntityFramework;
using BatuhanBulut.AppMonitor.DataAccess.Abstract;
using BatuhanBulut.AppMonitor.Entities;

namespace BatuhanBulut.AppMonitor.DataAccess.Concrete.EntityFramework
{
    public class EfDownRecordDal : EfEntityRepositoryBase<DownRecord, MonitorContext>, IDownRecordDal
    {
    }
}
