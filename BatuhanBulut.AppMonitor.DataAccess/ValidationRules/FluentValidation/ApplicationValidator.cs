﻿using System;
using BatuhanBulut.AppMonitor.Entities;
using FluentValidation;

namespace BatuhanBulut.AppMonitor.DataAccess.ValidationRules.FluentValidation
{
    public class ApplicationValidator: AbstractValidator<Application>
    {
        public ApplicationValidator()
        {
            RuleFor(e => e.IsDeleted).Equal(false);
            RuleFor(e => e.URL)
                .Must(url => Uri.TryCreate(url, UriKind.Absolute, out _))
                .When(e => !string.IsNullOrWhiteSpace(e.URL));
            RuleFor(e => e.Name).NotEmpty();
        }
    }
}
