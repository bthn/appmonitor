﻿using System;
using BatuhanBulut.AppMonitor.Entities;
using FluentValidation;

namespace BatuhanBulut.AppMonitor.DataAccess.ValidationRules.FluentValidation
{
    public class UserValidator: AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(e => e.Name).NotEmpty();
            RuleFor(e => e.Surname).NotEmpty();
            RuleFor(e => e.Email).EmailAddress();
        }
    }
}
