﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using BatuhanBulut.AppMonitor.Core.Entities;

namespace BatuhanBulut.AppMonitor.Core.DataAccess
{
    public interface IEntityRepository<T> where T:class, IEntity, new()
    {
        List<T> GetList(Expression<Func<T, bool>> filter = null, bool eagerLoad = false);

        T Get(Expression<Func<T, bool>> filter, bool eagerLoad = false);

        T Add(T entity);

        void AddRange(List<T> entities);

        T Update(T entity);

        void Delete(T entity);
    }
}
