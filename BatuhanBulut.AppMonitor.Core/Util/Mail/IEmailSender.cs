﻿using System;
using System.Collections.Generic;

namespace BatuhanBulut.AppMonitor.Core.Util.Mail
{
    public interface IEmailSender
    {
        void SendEmail(List<string> to, string subject, string message);
    }
}
