﻿using System;
using System.Collections.Generic;
using System.Linq;
using MailKit.Net.Smtp;
using MimeKit;

namespace BatuhanBulut.AppMonitor.Core.Util.Mail
{
    public class EmailSender : IEmailSender
    {
        readonly EmailConfiguration emailConfiguration;
        public EmailSender(EmailConfiguration emailConfiguration)
        {
            this.emailConfiguration = emailConfiguration;
        }

        public void SendEmail(List<string> to, string subject, string message)
        {
            var email = new MimeMessage();
            email.From.Add(new MailboxAddress(emailConfiguration.FromAddress));
            email.To.AddRange(to.Select(e => new MailboxAddress(e)));
            email.Subject = subject;

            email.Body = new TextPart(MimeKit.Text.TextFormat.Plain)
            {
                Text = message
            };

            using (var emailClient = new SmtpClient())
            {
                emailClient.Connect(emailConfiguration.SmtpServer,
                    emailConfiguration.SmtpPort, true);

                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                emailClient.Authenticate(emailConfiguration.SmtpUsername,
                    emailConfiguration.SmtpPassword);

                emailClient.Send(email);
                emailClient.Disconnect(true);
            }
        }
    }
}
