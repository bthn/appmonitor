﻿using System;
using System.Net;

namespace BatuhanBulut.AppMonitor.Core.Util
{
    public class WebSiteStatusChecker
    {
        public bool IsUp(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Timeout = 5000;
            request.Method = "HEAD"; // As per Lasse's comment
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    return IsResponseCodeSuccess((int)response.StatusCode);
                }
            }
            catch (WebException)
            {
                return false;
            }
        }

        private bool IsResponseCodeSuccess(int responseCode)
        {
            return responseCode >= 200 && responseCode < 300;
        }
    }
}
