﻿using System;
using System.Collections.Generic;

namespace BatuhanBulut.AppMonitor.Core.CrossCuttingConcerns.Logging
{
    public class LogDetail
    {
        public string FullName { get; set; }
        public string MethodName { get; set; }
        public List<LogParameter> LogParameters { get; set; }
        public string Description { get; set; }
    }
}
