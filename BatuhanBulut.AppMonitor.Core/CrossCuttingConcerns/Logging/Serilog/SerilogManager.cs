﻿using System;
using Serilog;

namespace BatuhanBulut.AppMonitor.Core.CrossCuttingConcerns.Logging.Serilog
{
    public class SerilogManager : ILogService
    {
        public void Debug(string message)
        {
            Log.Debug(message);
        }

        public void Error(string message)
        {
            Log.Error(message);
        }

        public void Fatal(string message)
        {
            Log.Fatal(message);
        }

        public void Info(string message)
        {
            Log.Information(message);
        }

        public void Verbose(string message)
        {
            Log.Verbose(message);
        }

        public void Warning(string message)
        {
            Log.Warning(message);
        }
    }
}
