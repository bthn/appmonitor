﻿using System;
using System.Collections.Generic;
using BatuhanBulut.AppMonitor.Business.Abstract;
using BatuhanBulut.AppMonitor.DataAccess.Abstract;
using BatuhanBulut.AppMonitor.Entities;

namespace BatuhanBulut.AppMonitor.Business.Concrete
{
    public class DownRecordService : IDownRecordService
    {
        readonly IDownRecordDal downRecordDal;

        public DownRecordService(IDownRecordDal downRecordDal)
        {
            this.downRecordDal = downRecordDal;
        }

        public void Add(DownRecord downRecord)
        {
            downRecordDal.Add(downRecord);
        }

        public DownRecord Get(long id)
        {
            return downRecordDal.Get(e => e.Id == id);
        }

        public List<DownRecord> GetTodaysRecords()
        {
            return downRecordDal.GetList(e => e.CreateDate >= DateTime.Today.AddDays(-1));
        }
    }
}
