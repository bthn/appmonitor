﻿using System;
using BatuhanBulut.AppMonitor.Business.Abstract;
using BatuhanBulut.AppMonitor.DataAccess.Abstract;
using BatuhanBulut.AppMonitor.Entities;

namespace BatuhanBulut.AppMonitor.Business.Concrete
{
    public class UserService : IUserService
    {
        readonly IUserDal userDal;

        public UserService(IUserDal userDal)
        {
            this.userDal = userDal;
        }

        public void Add(User user)
        {
            userDal.Add(user);
        }

        public void Delete(long id)
        {
            var user = userDal.Get(e => e.Id == id);
            user.IsDeleted = true;
            userDal.Update(user);
        }
    }
}
