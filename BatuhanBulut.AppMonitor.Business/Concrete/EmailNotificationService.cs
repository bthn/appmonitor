﻿using System;
using System.Linq;
using System.Text;
using BatuhanBulut.AppMonitor.Business.Abstract;
using BatuhanBulut.AppMonitor.Core.Util.Mail;
using BatuhanBulut.AppMonitor.DataAccess.Abstract;
using BatuhanBulut.AppMonitor.Entities;

namespace BatuhanBulut.AppMonitor.Business.Concrete
{
    public class EmailNotificationService: INotificationService
    {
        readonly ITargetEmailDal targetEmailDal;
        readonly IEmailSender emailSender;

        public EmailNotificationService(ITargetEmailDal targetEmailDal,
            IEmailSender emailSender)
        {
            this.targetEmailDal = targetEmailDal;
            this.emailSender = emailSender;
        }

        public void Notify(Application application)
        {
            var targetMails = targetEmailDal.GetList(e => e.ApplicationId == application.Id);

            var message = string.Format("URL: {0}\nName: {1}\nTime: {2}",
                    application.URL, application.Name, application.CreateDate);

            var mailAddresses = targetMails.Select(e => e.Email).ToList();

            emailSender.SendEmail(mailAddresses, application.Name, message);
        }
    }
}
