﻿using System;
using System.Collections.Generic;
using BatuhanBulut.AppMonitor.Business.Abstract;
using BatuhanBulut.AppMonitor.Core.Util;
using BatuhanBulut.AppMonitor.DataAccess.Abstract;
using BatuhanBulut.AppMonitor.Entities;

namespace BatuhanBulut.AppMonitor.Business.Concrete
{
    public class ApplicationService : IApplicationService
    {
        readonly IApplicationDal applicationDal;
        readonly INotificationService notificationService;
        readonly ISentFlagDal sentFlagDal;
        readonly IDownRecordDal downRecordDal;
        readonly WebSiteStatusChecker statusChecker;

        public ApplicationService(IApplicationDal applicationDal,
            INotificationService notificationService,
            ISentFlagDal sentFlagDal,
            IDownRecordDal downRecordDal,
            WebSiteStatusChecker statusChecker)
        {
            this.applicationDal = applicationDal;
            this.notificationService = notificationService;
            this.sentFlagDal = sentFlagDal;
            this.downRecordDal = downRecordDal;
            this.statusChecker = statusChecker;
        }

        public void Add(Application application)
        {
            applicationDal.Add(application);
            //TODO: hook it to the scheduler here
        }

        public void ControlStatus(long id)
        {
            var application = applicationDal.Get(e => e.Id == id);

            bool isUp = statusChecker.IsUp(application.URL);
            var flag = sentFlagDal.Get(e => e.ApplicationId == id);
            if (isUp)
            {
                if(flag != null)
                {
                    sentFlagDal.Delete(flag);
                }
            }
            else
            {
                if(flag == null)
                {
                    downRecordDal.Add(new DownRecord
                    {
                        ApplicationId = id
                    });
                    notificationService.Notify(application);
                    flag = new SentFlag { ApplicationId = id };
                    sentFlagDal.Add(flag);
                }
            }
        }

        public void Delete(long id)
        {
            var application = applicationDal.Get(e => e.Id == id);
            application.IsDeleted = true;
            applicationDal.Update(application);
        }

        public List<Application> GetAllApplications()
        {
            return applicationDal.GetList();
        }
    }
}
