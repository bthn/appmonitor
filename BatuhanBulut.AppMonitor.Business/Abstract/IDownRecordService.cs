﻿using System;
using System.Collections.Generic;
using BatuhanBulut.AppMonitor.Entities;

namespace BatuhanBulut.AppMonitor.Business.Abstract
{
    public interface IDownRecordService
    {
        List<DownRecord> GetTodaysRecords();
        void Add(DownRecord downRecord);
        DownRecord Get(long id);
    }
}
