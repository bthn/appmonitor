﻿using System;
using BatuhanBulut.AppMonitor.Entities;

namespace BatuhanBulut.AppMonitor.Business.Abstract
{
    public interface IUserService
    {
        void Add(User user);
        void Delete(long id);
    }
}
