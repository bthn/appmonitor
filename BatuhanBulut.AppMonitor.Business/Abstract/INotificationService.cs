﻿using System;
using BatuhanBulut.AppMonitor.Entities;

namespace BatuhanBulut.AppMonitor.Business.Abstract
{
    public interface INotificationService
    {
        void Notify(Application application);
    }
}
