﻿using System;
using System.Collections.Generic;
using BatuhanBulut.AppMonitor.Entities;

namespace BatuhanBulut.AppMonitor.Business.Abstract
{
    public interface IApplicationService
    {
        List<Application> GetAllApplications();
        void Add(Application application);
        void Delete(long id);
        void ControlStatus(long id);
    }
}
