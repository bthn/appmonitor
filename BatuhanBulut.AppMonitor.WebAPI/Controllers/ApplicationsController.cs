﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuhanBulut.AppMonitor.Business.Abstract;
using BatuhanBulut.AppMonitor.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BatuhanBulut.AppMonitor.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ApplicationsController : ControllerBase
    {
        readonly IApplicationService applicationService;

        public ApplicationsController(IApplicationService applicationService)
        {
            this.applicationService = applicationService;
        }

        [HttpGet]
        public IEnumerable<Application> Get()
        {
            return applicationService.GetAllApplications();
        }


        [HttpPost]
        public void Post(Application application)
        {
            applicationService.Add(application);
        }

        [HttpDelete("{id}")]
        public void Delete(long id)
        {
            applicationService.Delete(id);
        }
    }
}
