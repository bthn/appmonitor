﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuhanBulut.AppMonitor.Business.Abstract;
using BatuhanBulut.AppMonitor.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BatuhanBulut.AppMonitor.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }


        [HttpPost]
        public void Post(User user)
        {
            userService.Add(user);
        }

        [HttpDelete("{id}")]
        public void Delete(long id)
        {
            userService.Delete(id);
        }
    }
}
