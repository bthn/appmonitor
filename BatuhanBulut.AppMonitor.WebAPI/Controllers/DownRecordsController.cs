﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuhanBulut.AppMonitor.Business.Abstract;
using BatuhanBulut.AppMonitor.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace BatuhanBulut.AppMonitor.WebAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DownRecordsController : ControllerBase
    {
        readonly IDownRecordService downRecordService;

        public DownRecordsController(IDownRecordService downRecordService)
        {
            this.downRecordService = downRecordService;
        }

        [HttpGet]
        public IEnumerable<DownRecord> Get()
        {
            return downRecordService.GetTodaysRecords();
        }

        [HttpGet("{id}")]
        public DownRecord Get(long id)
        {
            return downRecordService.Get(id);
        }

        [HttpPost]
        public void Post(DownRecord value)
        {
            downRecordService.Add(value);
        }

    }
}
