using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BatuhanBulut.AppMonitor.Business.Abstract;
using BatuhanBulut.AppMonitor.Business.Concrete;
using BatuhanBulut.AppMonitor.Core.Util;
using BatuhanBulut.AppMonitor.Core.Util.Mail;
using BatuhanBulut.AppMonitor.DataAccess.Abstract;
using BatuhanBulut.AppMonitor.DataAccess.Concrete.EntityFramework;
using BatuhanBulut.AppMonitor.DataAccess.ValidationRules.FluentValidation;
using BatuhanBulut.AppMonitor.Entities;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace BatuhanBulut.AppMonitor.WebAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //fill DI container
            services.AddTransient<IUserDal, EfUserDal>()
                .AddTransient<IApplicationDal, EfApplicationDal>()
                .AddTransient<IDownRecordDal, EfDownRecordDal>()
                .AddTransient<ITargetEmailDal, EfTargetEmailDal>()
                .AddTransient<ISentFlagDal, EfSentFlagDal>()
                .AddTransient<DbContext, MonitorContext>()
                .AddTransient<IUserService, UserService>()
                .AddTransient<IApplicationService, ApplicationService>()
                .AddTransient<IDownRecordService, DownRecordService>()
                .AddTransient<INotificationService, EmailNotificationService>()
                .AddTransient<WebSiteStatusChecker>()
                .AddSingleton(
                    Configuration.GetSection("EmailConfiguration")
                        .Get<EmailConfiguration>())
                .AddTransient<IEmailSender, EmailSender>()
                .AddSingleton<IValidator<User>, UserValidator>()
                .AddSingleton<IValidator<Application>, ApplicationValidator>()
                .AddSingleton<IValidator<DownRecord>, DownRecordValidator>();

            services.AddEntityFrameworkNpgsql()
                .AddDbContext<MonitorContext>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
