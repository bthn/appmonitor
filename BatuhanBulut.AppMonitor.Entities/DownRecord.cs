﻿using System;
using BatuhanBulut.AppMonitor.Core.Entities;

namespace BatuhanBulut.AppMonitor.Entities
{
    public class DownRecord : IEntity
    {
        public DownRecord()
        {
            CreateDate = DateTime.Now;
        }
        public long Id { get; set; }
        public long ApplicationId { get; set; }
        public Application Application { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
