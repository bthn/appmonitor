﻿using System;
using BatuhanBulut.AppMonitor.Core.Entities;

namespace BatuhanBulut.AppMonitor.Entities
{
    public class SentFlag : IEntity
    {
        public long Id { get; set; }
        public long ApplicationId { get; set; }
        public Application Application { get; set; }
    }
}
