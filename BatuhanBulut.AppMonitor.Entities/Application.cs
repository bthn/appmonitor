﻿using System;
using BatuhanBulut.AppMonitor.Core.Entities;

namespace BatuhanBulut.AppMonitor.Entities
{
    public class Application : IEntity
    {
        public Application()
        {
            IsDeleted = false;
            CreateDate = DateTime.Now;
        }
        public long Id { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public long CheckIntervalInSeconds { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
