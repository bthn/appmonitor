﻿using System;
using BatuhanBulut.AppMonitor.Core.Entities;

namespace BatuhanBulut.AppMonitor.Entities
{
    public class TargetEmail : IEntity
    {
        public TargetEmail()
        {
            IsDeleted = false;
            CreateDate = DateTime.Now;
        }

        public long Id { get; set; }
        public long ApplicationId { get; set; }
        public Application Application { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
