﻿using System;
using BatuhanBulut.AppMonitor.Core.Entities;

namespace BatuhanBulut.AppMonitor.Entities
{
    public class User : IEntity
    {
        public User()
        {
            IsDeleted = false;
            CreateDate = DateTime.Now;
        }
        public long Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
